using System.Reflection.Emit;
using System.Collections.Concurrent;
using System.Data;
using Internal;
using System;

enum Time
{
  hour = 1,
  week = 2,
  year = 3,
}



class Wage_convert
{
    static void Main ()
    {
      string time_prompt =
        @"Enter wage rate units. Use
         '1' for hourly;
         '2' for weekly;
         '3' for yearly: ";

      decimal pay = 0.000M;

      Console.Write ("Enter wage rate: ");
      pay = decimal.Parse ( Console.ReadLine() );

      Console.Write (time_prompt);
      int units = int.Parse( Console.ReadLine() );
      convert_wage(pay, units);
  }

   static void convert_wage(decimal pay, int units)
  {

    decimal[] hourly = new decimal[5];
    decimal[] weekly = new decimal[5];
    decimal[] yearly = new decimal[5];
    decimal[] bill_rate = new decimal[4];

    string default_msg = "Only valid entries are: \n 'h' for hourly; \n 'w' for weekly; \n 'y' for yearly.";


    switch(units)
    {
            case 1: { hourly[0] = pay;            weekly[0] = (pay * 40);  yearly[0] = (pay * 2080); break; }
            case 2: { hourly[0] = (pay / 40);     weekly[0] = pay;         yearly[0] = (pay * 52);   break; }
            case 3: { hourly[0] = (pay/ 2080);    weekly[0] = (pay / 52);  yearly[0] = pay;          break; }
            default:  { Console.WriteLine(default_msg) ;                                             break; }

    }

    bill_rate[0] = 0.7M;
    bill_rate[1] = (0.7M * 0.8M);
    bill_rate[2] = (0.7M * 0.75M);
    bill_rate[3] = (0.7M * 0.70M);

    // Total Compensation infered as percentage of wage.  Column 0 of wage table is initial conversion of
    // user input.  Columns 1-4 will be "Total Compensation, Total Comp + X% margin" respectively.
    // bill_rate[0] = Total Comp, bill_rate[1] = total com * 20% margin, etc.

    for (int i = 0; i < bill_rate.Length; i++ )
    {
      hourly[i+1] = hourly[0] / bill_rate[i];
      weekly[i+1] = weekly[0] / bill_rate[i];
      yearly[i+1] = yearly[0] / bill_rate[i];
    }

    //  Data and Formatting assignments for table output.

    string[] hour_header = new string[] {"Hourly", "With Benefits", "20% Margin", "25% Margin", "30% Margin"};
    string[] week_header = new string[] {"Weekly", "With Benefits", "20% Margin", "25% Margin", "30% Margin"};
    string[] year_header = new string[] {"Yearly", "With Benefits", "20% Margin", "25% Margin", "30% Margin"};

    string header_template =   "   {0, -12}   {1,-15}  {2, -15}  {3,-15}  {4,-15}";
    string number_template =   "   {0, -12:C}   {1,-15:C}  {2, -15:C}  {3,-15:C} {4,-15:C}\n";

    string hour_table = String.Format(header_template, hour_header);
    string hour_data  = String.Format(number_template, hourly[0], hourly[1], hourly[2], hourly[3], hourly[4]);

    string week_table = String.Format(header_template, week_header);
    string week_data  = String.Format(number_template, weekly[0], weekly[1], weekly[2], weekly[3], weekly[4]);

    string year_table = String.Format(header_template, year_header);
    string year_data  = String.Format(number_template, yearly[0], yearly[1], yearly[2], yearly[3], yearly[4]);

    Console.WriteLine();
    Console.WriteLine(hour_table);
    Console.WriteLine(hour_data);
    Console.WriteLine(week_table);
    Console.WriteLine(week_data);
    Console.WriteLine(year_table);
    Console.WriteLine(year_data);


//    Console.WriteLine($"You entered {pay} per {Enum.GetName(typeof(Time), units) }");
  }
}