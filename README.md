Wage Converter: A command line utility for people who work through agencies in the US.

This little application was inspired by my talks with various agencies for medical and allied health professionals.  For example,
it is common to discuss hourly rates directly with facilities, but travel agencies will quote weekly rates.  Sometimes one will have to
compare the full time salary with either an hourly quote, or a weekly quote.  This utility does all of that.  

It isn't perfect.  Appropriate adjustments should be made to calculate after-tax wages, but that is complex.  These are good pre-tax estimates
though.

To compile, make sure you have the C# compiler in your PATH.  

csc.exe wage_converter.cs /out:<your_name_here>  
